// File: louvain.h
// -- community detection header file
//-----------------------------------------------------------------------------
// Community detection
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// This file is part of Louvain algorithm.
// 
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : February 2008
//-----------------------------------------------------------------------------
// see readme.txt for more details

#ifndef LOUVAIN_H
#define LOUVAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <unordered_set>
#include <map>

#include "graph_binary.h"
#include "quality.h"
#include "louvain.h"
#include "similarity.h"

using namespace std;


class Louvain {
 public:
  vector<long double> neigh_weight;
  vector<long double> neigh_sim;
  vector<int> neigh_pos;
  int neigh_last;
  double alpha;

  // number of pass for one level computation
  // if -1, compute as many pass as needed to increase quality
  int nb_pass;

  // a new pass is computed if the last one has generated an increase 
  // better than eps_impr
  // if 0.0L even a minor increase is enough to go for one more pass
  long double eps_impr;
  
  // Quality functions used to compute communities
  Quality* qual;
  
  // Similarity functions used for attribute simliarity
  Similarity* sim;

  // constructors:
  // reads graph from file using graph constructor
  // type defined the weighted/unweighted status of the graph file
  Louvain (double alpha, int nb_pass, long double eps_impr, Quality* q, Similarity* s);

  // initiliazes the partition with something else than all nodes alone
  void init_partition(const char *filename_part);
  void init_partition(vector<int> &n2c_new);

  // compute the set of neighboring communities of node
  // for each community, gives the number of links from node to comm
  // also gives the similarity from node to comm
  void neigh_comm(int node, bool compute_sim=true);

  // calculates total similarity between node and all nodes in comm
  long double node_comm_sim(int node, int comm);

  // displays the graph of communities as computed by one_level
  void partition2graph();

  // displays the current partition (with communities renumbered from 0 to k-1)
  static void display_partition(vector<int>&);

  // generates the binary graph of communities as computed by one_level
  Graph partition2graph_binary();

  // compute communities of the graph for one level
  // return true if some nodes have been moved
  bool one_level();

  // composite score: combination of quality and similarity
  long double composite();
  
  // renumber community IDs to be consecutive
  void renumber_comms();
};


#endif // LOUVAIN_H
