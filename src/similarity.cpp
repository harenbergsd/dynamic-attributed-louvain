#include<dirent.h>
#include <sys/stat.h>
#include "similarity.h"

using namespace std::placeholders;


Similarity::Similarity(string fname, string sim_type) {
    if (sim_type == "jaccard")
        func = std::bind(&Similarity::jaccard, this, _1, _2);
    else
        func = std::bind(&Similarity::hamming, this, _1, _2, _3);
    initialize(fname);
}


void Similarity::initialize(string filename) {
    if (filename == "")
        attrs_set = aggregate_attrs();
    else
        read_attrs_set(filename);
    
    size = attrs_set[0].size();
    c2n = vector<vector<int> >(size);
    comm_sim = vector<long double>(size, 1);
    
    // Each node community contains one node
    ncomms = size;
    for (int i=0; i<size; i++)
        c2n[i].push_back(i);
}


/* Combine the attributes of all nodes in a community.
 * For each attribute, the mode over all nodes becomes the attribute of the community meta-node. */
vector<attrs> Similarity::aggregate_attrs() {
    vector<attrs> new_attrs_set;
    for (unsigned i=0; i<attrs_set.size(); i++) {
        attrs atts;
        for (unsigned j=0; j<c2n.size(); j++) {
            vector<int> comm = c2n[j];
            if (comm.size() == 0)
                continue;
            vector<int> combined(nattrs[i], 0);
            atts.push_back(bitset<MAX_DIM>());
            for (auto node : comm)
                for (int k=0; k<nattrs[i]; k++)
                    combined[k] += attrs_set[i][node][k];
            for (int k=0; k<nattrs[i]; k++)
                atts[j][k] = ((combined[k]*2)/comm.size()==0)?0:1;
        }
        new_attrs_set.push_back(atts);
    }
    return new_attrs_set;
}


/* Calculate global similarity using internal fields */
long double Similarity::similarity() {
    long double tot_similarity = 0.0;
    for (int c=0; c<size; c++){
        int csize = c2n[c].size();
        if (csize < 1)
            continue;
        tot_similarity += comm_sim[c]/(csize*csize);
    }
    return tot_similarity/ncomms;
}


/* Calculate global similarity using given community map and similarity values */
long double Similarity::similarity(vector<vector<int> > &c2n, vector<long double> &comm_sim) {
    long double tot_similarity = 0.0;
    for (unsigned c=0; c<c2n.size(); c++){
        int csize = c2n[c].size();
        if (csize < 1)
            continue;
        tot_similarity += comm_sim[c]/(csize*csize);
    }
    return tot_similarity/c2n.size();
}


/* Read each node's attribute (single line) into a bit vector */
vector<bitset<MAX_DIM> >
Similarity::read_attrs(string filename){
    ifstream infile(filename);
    if (!infile.is_open())
        exit(1);
    
    int n, i=0;
    string line;
    vector<bitset<MAX_DIM> > attrs;
    while (getline(infile, line))
    {
        bitset<MAX_DIM> v;
        istringstream iss(line);
        i=0;
        while (iss >> n) {
            v[i] = n>0?1:0;
            i++;
        }
        attrs.push_back(v);
    }
    nattrs.push_back(i);
    infile.close();
    return attrs;
}


/* Each file holds one attribute for all the nodes. 
 * This function loads each file (attribute) in a given directory.
 * If fname is not a directory, it will just load that individual file. */
void Similarity::read_attrs_set(string fname){
    struct stat path_stat;
    stat(fname.c_str(), &path_stat);
    if (S_ISREG(path_stat.st_mode))
        attrs_set.push_back(read_attrs(fname));
    else {
        DIR *dirp = opendir(fname.c_str());
        dirent *dp;
        vector<string> filenames;
        while ((dp = readdir(dirp)) != NULL)
            if (strcmp(dp->d_name,".") && strcmp(dp->d_name,".."))
                filenames.push_back(fname+"/"+dp->d_name);
        sort(filenames.begin(), filenames.end());

        for (auto f : filenames)
            attrs_set.push_back(read_attrs(f));
    }
}
