// File: louvain.cpp
// -- community detection source file
//-----------------------------------------------------------------------------
// Community detection
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// This file is part of Louvain algorithm.
// 
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : February 2008
//-----------------------------------------------------------------------------
// see readme.txt for more details

#include "louvain.h"

using namespace std;


Louvain::Louvain(double alpha, int nbp, long double epsq, Quality* q, Similarity* s) {
  this->alpha = alpha;
  qual = q;
  sim = s;

  neigh_weight.resize(qual->size,-1);
  neigh_sim.resize(qual->size, -1);
  neigh_pos.resize(qual->size);
  neigh_last = 0;

  nb_pass = nbp;
  eps_impr = epsq;
}

void
Louvain::init_partition(const char * filename) {
  ifstream finput;
  finput.open(filename,fstream::in);

  // read partition
  while (!finput.eof()) {
    int node, comm;
    finput >> node >> comm;
    
    if (finput) {
      int old_comm = qual->n2c[node];
      neigh_comm(node, false);
      neigh_sim[old_comm] = 2*node_comm_sim(node, old_comm);
      neigh_sim[comm] = 2*node_comm_sim(node, comm);

      qual->remove(node, old_comm, neigh_weight[old_comm]);
      sim->remove(node, old_comm, neigh_sim[old_comm]);
      
      int i=0;
      for (i=0 ; i<neigh_last ; i++) {
	    int best_comm = neigh_pos[i];
	    if (best_comm==comm) {
	      qual->insert(node, best_comm, neigh_weight[comm]);
	      sim->insert(node, best_comm, neigh_sim[comm]);
	      break;
	    }
      }
      if (i==neigh_last) { // in community with no direct neighbors
        neigh_sim[comm] = 0;
        for (unsigned neigh : sim->c2n[comm])
          neigh_sim[comm] += 2*sim->sim(node, neigh);
	    qual->insert(node, comm, 0);
	    sim->insert(node, comm, neigh_sim[comm]);
      }
    }
  }
  finput.close();
}


void
Louvain::neigh_comm(int node, bool compute_sim) {
  for (int i=0 ; i<neigh_last ; i++) {
    neigh_weight[neigh_pos[i]]=-1;
    neigh_sim[neigh_pos[i]]=0;
  }
  
  neigh_last = 0;

  pair<vector<int>::iterator, vector<long double>::iterator> p = (qual->g).neighbors(node);
  int deg = (qual->g).nb_neighbors(node);

  neigh_pos[0] = qual->n2c[node];
  neigh_weight[neigh_pos[0]] = 0;
  neigh_sim[neigh_pos[0]] = 0;
  neigh_last = 1;

  unordered_set<int> comms;
  comms.insert(neigh_pos[0]);
  for (int i=0 ; i<deg ; i++) {
    int neigh  = *(p.first+i);
    int neigh_comm = qual->n2c[neigh];
    long double neigh_w = ((qual->g).weights.size()==0)?1.0L:*(p.second+i);
    
    if (neigh!=node) {
      comms.insert(neigh_comm);
      if (neigh_weight[neigh_comm]==-1) {
	    neigh_weight[neigh_comm] = 0.0L;
	    neigh_sim[neigh_comm] = 0.0L;
	    neigh_pos[neigh_last++] = neigh_comm;
      }
      neigh_weight[neigh_comm] += neigh_w;
    }
  }

  if (compute_sim)
    for (auto c : comms)
      neigh_sim[c] = 2*node_comm_sim(node, c);
}


// Compute the similarity of the node with the members of the community
long double 
Louvain::node_comm_sim(int u, int comm) {
    long double val = 0.0;
    for (int v: sim->c2n[comm])
        if (u != v)
            val += sim->sim(u, v);
    return val;
}


void
Louvain::partition2graph() {
  for (int i=0 ; i< qual->size ; i++) {
    pair<vector<int>::iterator, vector<long double>::iterator> p = (qual->g).neighbors(i);

    int deg = (qual->g).nb_neighbors(i);
    for (int j=0 ; j<deg ; j++) {
      int neigh = *(p.first+j);
      cout << qual->n2c[i] << " " << qual->n2c[neigh] << endl;
    }
  }
}

void
Louvain::display_partition(vector<int> &n2c) {
  for (unsigned i=0 ; i < n2c.size(); i++)
    cout << i << " " << n2c[i] << endl;
}

long double
Louvain::composite() {
    return alpha*qual->quality() + (1-alpha)*sim->similarity();
}

Graph
Louvain::partition2graph_binary() {
  // Compute communities
  int last = sim->c2n.size();
  vector<vector<int> > comm_nodes(last);
  vector<int> comm_weight(last, 0);
  
  for (int node = 0 ; node < (qual->size) ; node++) {
    comm_nodes[qual->n2c[node]].push_back(node);
    comm_weight[qual->n2c[node]] += (qual->g).nodes_w[node];
  }

  // Compute weighted graph
  Graph g2;
  int nbc = comm_nodes.size();

  g2.nb_nodes = comm_nodes.size();
  g2.degrees.resize(nbc);
  g2.nodes_w.resize(nbc);
  
  for (int comm=0 ; comm<nbc ; comm++) {
    map<int,long double> m;
    map<int,long double>::iterator it;

    int size_c = comm_nodes[comm].size();

    g2.assign_weight(comm, comm_weight[comm]);

    for (int node=0 ; node<size_c ; node++) {
      pair<vector<int>::iterator, vector<long double>::iterator> p = (qual->g).neighbors(comm_nodes[comm][node]);
      int deg = (qual->g).nb_neighbors(comm_nodes[comm][node]);
      for (int i=0 ; i<deg ; i++) {
    int neigh = *(p.first+i);
    int neigh_comm = qual->n2c[neigh];
    long double neigh_weight = ((qual->g).weights.size()==0)?1.0L:*(p.second+i);

    it = m.find(neigh_comm);
    if (it==m.end())
      m.insert(make_pair(neigh_comm, neigh_weight));
    else
      it->second += neigh_weight;
      }
    }

    g2.degrees[comm] = (comm==0)?m.size():g2.degrees[comm-1]+m.size();
    g2.nb_links += m.size();

    for (it = m.begin() ; it!=m.end() ; it++) {
      g2.total_weight += it->second;
      g2.links.push_back(it->first);
      g2.weights.push_back(it->second);
    }
  }

  return g2;
}

bool
Louvain::one_level() {
  bool improvement=false ;
  int nb_moves;
  int nb_pass_done = 0;
  //long double new_qual = qual->quality();
  long double new_qual = composite();
  long double cur_qual = new_qual;

  vector<int> random_order(qual->size);
  for (int i=0 ; i < qual->size ; i++)
    random_order[i]=i;
  for (int i=0 ; i < qual->size-1 ; i++) {
    int rand_pos = rand()%(qual->size-i)+i;
    int tmp = random_order[i];
    random_order[i] = random_order[rand_pos];
    random_order[rand_pos] = tmp;
  }

  // repeat while 
  //   there is an improvement of quality
  //   or there is an improvement of quality greater than a given epsilon 
  //   or a predefined number of pass have been done
  do {
    cur_qual = new_qual;
    nb_moves = 0;
    nb_pass_done++;

    // for each node: remove the node from its community and insert it in the best community
    for (int node_tmp = 0 ; node_tmp < qual->size ; node_tmp++) {
      //int node = random_order[node_tmp];
      int node = node_tmp;
      int node_comm = qual->n2c[node];
      long double w_degree = (qual->g).weighted_degree(node);

      // computation of all neighboring communities of current node
      neigh_comm(node);
      // remove node from its current community
      qual->remove(node, node_comm, neigh_weight[node_comm]);
      sim->remove(node, node_comm, neigh_sim[node_comm]);


      // compute the nearest community for node
      // default choice for future insertion is the former community
      int best_comm = node_comm;
      long double best_nblinks  = 0.0L;
      long double best_increase = 0.0L;
      for (int i=0 ; i<neigh_last ; i++) {
	    long double mod_increase = qual->gain(node, neigh_pos[i], neigh_weight[neigh_pos[i]], w_degree);
	    long double sim_increase = sim->gain(node, neigh_pos[i], neigh_sim[neigh_pos[i]]);
        if (sim_increase < 0)
            sim_increase = 0;
        long double increase = alpha*mod_increase + (1-alpha)*sim_increase;
	    if (increase>best_increase) {
	      best_comm = neigh_pos[i];
	      best_nblinks = neigh_weight[neigh_pos[i]];
	      best_increase = increase;
	    }
      }
      
      // insert node in the nearest community
      qual->insert(node, best_comm, best_nblinks);
      sim->insert(node, best_comm, neigh_sim[best_comm]);
     
      if (best_comm!=node_comm)
	    nb_moves++;
    }


    //new_qual = qual->quality();
    new_qual = composite();
    
    if (nb_moves>0)
      improvement=true;


  } while (nb_moves>0 && new_qual-cur_qual > eps_impr);

  return improvement;
}


void
Louvain::renumber_comms() {
  int size = qual->size;
  
  vector<int> renumber(size,-1);
  for (int node=0 ; node < size ; node++) 
    renumber[qual->n2c[node]]++;

  int end=0;
  for (int i=0 ; i < size ; i++)
    if (renumber[i]!=-1)
      renumber[i] = end++;
  
  // Renumber n2c mapping
  for (int i=0 ; i < size ; i++)
    qual->n2c[i] = renumber[qual->n2c[i]];

  // Renumber c2n mapping and update comm_sim accordingly
  vector<vector<int> > new_c2n(end);
  vector<long double> new_csim(end);
  for (int i=0 ; i < size ; i++) {
      if (renumber[i] != -1) {
        new_c2n[renumber[i]] = sim->c2n[i];
        new_csim[renumber[i]] = sim->comm_sim[i];
      }
  }
  sim->c2n = new_c2n;
  sim->comm_sim = new_csim;
}
