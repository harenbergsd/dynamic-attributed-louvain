// File: main_louvain.cpp
// -- community detection, sample main file
//-----------------------------------------------------------------------------
// Community detection 
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// And based on the article
// Copyright (C) 2013 R. Campigotto, P. Conde Céspedes, J.-L. Guillaume
//
// This file is part of Louvain algorithm.
// 
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume and R. Campigotto
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : July 2013
//-----------------------------------------------------------------------------
// see README.txt for more details


#include "graph_binary.h"
#include "louvain.h"
#include <unistd.h>
#include <string.h>
#include <time.h>

#include <algorithm>
#include <dirent.h>
#include <stdio.h>

#include "modularity.h"
#include "zahn.h"
#include "owzad.h"
#include "goldberg.h"
#include "condora.h"
#include "devind.h"
#include "devuni.h"
#include "dp.h"
#include "shimalik.h"
#include "balmod.h"

using namespace std;

long double precision = 0.000001L;  // minimum score change to warrant ending phase 1
unsigned short id_qual = 0;         // quality definition (e.g., modularity)

string dirname  = "";               // directory name of where the graph and attribute files are located
string sim_type  = "";              // similarity function, either hamming or jaccard
double alpha    = 1;                // weighting parameter between quality and similarity
double beta     = 1;                // change-node parameter: fraction of nodes to remove from their community
Quality *q;

// Parameters for various quality scores
long double ozp = 0.5L;
int kmin = 1;
long double sum_se = 0.0L;
long double sum_sq = 0.0L;
long double max_w = 1.0L;


void
usage(char *prog_name, const char *more) {
    cerr << more;
    cerr << "usage: " << prog_name << " dirname [-a alpha] [-b beta] [-s sim_type] [-q id_qual] [-c ozp] [-k min] [-w weight_file] [-p part_file] [-e epsilon] [-l display_level] [-v] [-h]" << endl << endl;
    cerr << "dirname: directory name where the graph and attribute files are located" << endl;
    
    cerr << "-a alph\tbalancing parameter [0,1] between structure (full=1) or attribute (0)" << endl;
    cerr << "-b beta\tfraction of vertices to change (0,1]." << endl;
    cerr << "-s func\tsimilarity function: 'hamming' or 'jaccard'" << endl;
    cerr << "-q id\tthe quality function used to compute partition of the graph (modularity is chosen by default):" << endl << endl;
    
    cerr << "\tid = 0\t -> the classical Newman-Girvan criterion (also called \"Modularity\")" << endl;
    cerr << "\tid = 1\t -> the Zahn-Condorcet criterion" << endl;
    cerr << "\tid = 2\t -> the Owsinski-Zadrozny criterion (you should specify the value of the parameter with option -c)" << endl;
    cerr << "\tid = 3\t -> the Goldberg Density criterion" << endl;
    cerr << "\tid = 4\t -> the A-weighted Condorcet criterion" << endl;
    cerr << "\tid = 5\t -> the Deviation to Indetermination criterion" << endl;
    cerr << "\tid = 6\t -> the Deviation to Uniformity criterion" << endl;
    cerr << "\tid = 7\t -> the Profile Difference criterion" << endl;
    cerr << "\tid = 8\t -> the Shi-Malik criterion (you should specify the value of kappa_min with option -k)" << endl;
    cerr << "\tid = 9\t -> the Balanced Modularity criterion" << endl;
    
    cerr << endl;
    cerr << "-c ozp\tthe parameter for the Owsinski-Zadrozny quality function (between 0.0 and 1.0: 0.5 is chosen by default)" << endl;
    cerr << "-k min\tthe kappa_min value (for Shi-Malik quality function) (it must be > 0: 1 is chosen by default)" << endl;
    cerr << endl;
    
    cerr << "-p file\tstart the computation with a given partition instead of the trivial partition" << endl;
    cerr << "\tfile must contain lines \"node community\"" << endl;
    cerr << "-e eps\ta given pass stops when the quality is increased by less than epsilon" << endl;
    cerr << "-h\tshow this usage message" << endl;
    
    exit(0);
}


void
parse_args(int argc, char **argv) {
    if (argc<2)
        usage(argv[0], "Bad arguments number\n");
    
    for (int i = 1; i < argc; i++) {
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'a':
                    alpha = atof(argv[i+1]);
                    i++;
                    break;
                case 's':
                    if (strcmp(argv[i+1], "hamming") && strcmp(argv[i+1], "jaccard"))
                        usage(argv[0], "Incorrect sim_type, need 'hamming' or 'jaccard'\n");
                    sim_type = argv[i+1];
                    i++;
                    break;
                case 'q':
                    id_qual = (unsigned short)atoi(argv[i+1]);
                    i++;
                    break;
                case 'c':
                    ozp = atof(argv[i+1]);
                    i++;
                    break;
                case 'k':
                    kmin = atoi(argv[i+1]);
                    i++;
                    break;
                case 'e':
                    precision = atof(argv[i+1]);
                    i++;
                    break;
                case 'b':
                    beta=atof(argv[i+1]);
                    i++;
                    break;
                case 'h':
                    usage(argv[0], "");
                    break;
                default:
                    usage(argv[0], "Unknown option\n");
            }
        } else {
            if (dirname == ""){
                dirname = argv[i];
                if (dirname.back() == '/')
                    dirname = dirname.substr(0, dirname.length()-1);
            }
            else
                usage(argv[0], "More than one filename\n");
        }
    }
    if (dirname == "")
        usage(argv[0], "No input file has been provided\n");
}


void
init_quality(Graph *g, unsigned short nbc) {
    if (nbc > 0)
        delete q;
    
    switch (id_qual) {
    case 0:
        q = new Modularity(*g);
        break;
    case 1:
        if (nbc == 0)
            max_w = g->max_weight();
        q = new Zahn(*g, max_w);
        break;
    case 2:
        if (nbc == 0)
            max_w = g->max_weight();
        if (ozp <= 0. || ozp >= 1.0)
            ozp = 0.5;
        q = new OwZad(*g, ozp, max_w);
        break;
    case 3:
        if (nbc == 0)
            max_w = g->max_weight();
        q = new Goldberg(*g, max_w);
        break;
    case 4:
        if (nbc == 0) {
            g->add_selfloops();
            sum_se = CondorA::graph_weighting(g);
        }
        q = new CondorA(*g, sum_se);
        break;
    case 5:
        q = new DevInd(*g);
        break;
    case 6:
        q = new DevUni(*g);
        break;
    case 7:
        if (nbc == 0) {
            max_w = g->max_weight();
            sum_sq = DP::graph_weighting(g);
        }
        q = new DP(*g, sum_sq, max_w);
        break;
    case 8:
        if (kmin < 1)
            kmin = 1;
        q = new ShiMalik(*g, kmin);
        break;
    case 9:
        if (nbc == 0)
            max_w = g->max_weight();
        q = new BalMod(*g, max_w);
        break;
    default:
        q = new Modularity(*g);
        break;
    }
}


/* Struct to hold statistics when running Louvain algorithm */
typedef struct {
    long double mod;
    long double sim;
    long double comp;
    int ncomms;
} Stats;


/* Main louvain algorithm */
Stats louvain(Graph &g, string afile, string pfile, vector<int> &n2c) {
    unsigned short nb_calls = 0;
    init_quality(&g, nb_calls);
    Similarity *s = new Similarity(afile, sim_type);
    nb_calls++;
    
    Louvain c(alpha, -1, precision, q, s);
    if (pfile != "")
        c.init_partition((char *)pfile.c_str());
    
    bool improvement = true;
    
    int level = 0;
    
    int size = q->size;
    Similarity *sfirst = new Similarity(afile, sim_type);
    vector<int> curr_n2c(size);     
    vector<vector<int> > c2n(size);
    for (int i=0; i<size; i++) {
        curr_n2c[i] = i;
        c2n[i].push_back(i);
    }
    n2c = curr_n2c;
    vector<long double> csim(size, 1); // community to which each node belongs
    Stats stats;
    stats.mod = q->quality();
    stats.sim = s->similarity();
    stats.comp = c.composite();
    do {
        // Phase 1
        improvement = c.one_level();
        c.renumber_comms();
    
        // Update the running n2c mapping
        int ncomms = -1;
        for (int n=0; n<size; n++) {
            curr_n2c[n] = q->n2c[curr_n2c[n]];
            if (curr_n2c[n] > ncomms)
                ncomms = curr_n2c[n];
        }
        ncomms ++;
        
        // Update c2n and scores
        vector<vector<int> > new_c2n(ncomms);
        vector<long double> new_csim(size, 0);
        for (int n=0; n<q->size; n++) {
            int cid = q->n2c[n];
            vector<int> *comm = &new_c2n[cid];
            
            new_csim[cid] += csim[n];
            for (auto u : *comm)
                for (auto v : c2n[n])
                    new_csim[cid] += 2*sfirst->sim(u,v);
            
            comm->reserve(comm->size()+c2n[n].size());
            comm->insert(comm->end(), c2n[n].begin(), c2n[n].end());
        }
        c2n = new_c2n;
        csim = new_csim;
        
        // Update some statistics
        long double mod = q->quality();
        long double sim = Similarity::similarity(c2n, csim);
        long double comp = alpha*mod + (1-alpha)*sim;
        if (comp >= stats.comp) {
            stats.mod = mod;
            stats.sim = sim;
            stats.comp = comp;
            stats.ncomms = ncomms;
            n2c = curr_n2c;
        }
        
        // Phase 2
        g = c.partition2graph_binary();
        init_quality(&g, nb_calls);
        s->initialize();
        nb_calls++;
        
        c = Louvain(alpha, -1, precision, q, s);
        
        if (pfile!="" && level==1) // do at least one more computation if partition is provided
            improvement=true;
    } while(improvement);

    return stats;
}


/* List filenames in given directory */
vector<string> get_filenames(string dir) {
    DIR *dirp = opendir(dir.c_str());
    dirent *dp;
    vector<string> filenames;
    while ((dp = readdir(dirp)) != NULL) {
        if (strcmp(dp->d_name,".") && strcmp(dp->d_name,".."))
            filenames.push_back(dir+"/"+dp->d_name);
    }
    sort(filenames.begin(), filenames.end());
    return filenames;
}


/* Write a partition to file with one node and the corresponding community ID on each line */
void write_partition(vector<int> partition, string filename) {
    ofstream file;
    file.open(filename);
    for (unsigned node=0; node<partition.size(); node++)
        file << node << " " << partition[node] << "\n";
    file.close();
}


/* Returns a sorted list of nodes that have changed the most based on structural and attribute changes */
vector<pair<int, long double> > rank_change_nodes(string g1, string a1, Graph &g_new, string a2) {
    Graph g = Graph((char *)g1.c_str(), NULL, UNWEIGHTED);
    Similarity *s= new Similarity(a1, sim_type);
    Similarity *s_new = new Similarity(a2, sim_type);
    
    vector<pair<int, long double> > change(g.nb_nodes);
    
    // Calculate the total change for each node (attribute and structure)
    for (int node=0; node<g.nb_nodes; node++) {
        
        // Calculate edge change
        vector<int> neighbors = g.neighbor_nodes(node);
        vector<int> new_neighbors = g_new.neighbor_nodes(node);
        long double edge_change = 0;
        if (neighbors.size()!=0 || new_neighbors.size()!=0) {
            vector<int> intersection;
            set_intersection(neighbors.begin(), neighbors.end(), 
                             new_neighbors.begin(), new_neighbors.end(), 
                             back_inserter(intersection));
            vector<int> union_set;
            set_union(neighbors.begin(), neighbors.end(), 
                             new_neighbors.begin(), new_neighbors.end(), 
                             back_inserter(union_set));
            edge_change = 1-((long double)intersection.size())/union_set.size();
        }

        // Calculate attribute change
        // Loop over and combine all types of attributes
        long double attr_change = 0;
        for (unsigned i=0; i<s->attrs_set.size(); i++)
            attr_change += s->func(s->attrs_set[i][node], s_new->attrs_set[i][node], s->nattrs[i]);
        attr_change = 1-attr_change/s->attrs_set.size();
        
        // Update change list
        change[node].first = node;
        change[node].second = alpha*edge_change + (1-alpha)*attr_change;
    }

    // Sort change list by value primarily (second element in pair) and by id secondarily
    sort(change.begin(), change.end(),[]
        (const pair<int,long double> &left, const pair<int,long double> &right) {
            if (left.second==right.second) {return (left.first < right.first);}
            return (left.second > right.second); });

    return change;
}


int
main(int argc, char **argv) {
    //srand(time(NULL)+getpid());
    srand(0);
    
    parse_args(argc, argv);
    
    // Get the graph and attribute datasets
    vector<string> afiles = get_filenames(dirname+"/attributes");
    vector<string> gfiles = get_filenames(dirname+"/graphs");
    int n = gfiles.size();
    int num_digits = to_string(n).length();
    
    // Iterate over all graphs and compute communities
    timespec start, tot_start, end;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tot_start);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    string partition_file = "";
    Graph graph = Graph((char *)gfiles[0].c_str(), NULL, UNWEIGHTED);
    for (int i=0; i<n; i++) {
        // Run louvain on static
        vector<int> n2c;
        Stats stats = louvain(graph, afiles[i], partition_file, n2c);

        // Write best partition to a file
        ostringstream os;
        os << setw(num_digits) << setfill('0') << to_string(i);
        write_partition(n2c, dirname + "/comm_" + os.str());
        
        // Display results
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
        double elapsed = (end.tv_sec-start.tv_sec)+(double)(end.tv_nsec-start.tv_nsec)/1000000000;
        cout.precision(5);
        cout << stats.ncomms << "," 
             << fixed << stats.mod << "," 
             << fixed << stats.sim << ","
             << fixed << stats.comp << ","
             << fixed << elapsed << endl;

        
        // If we are on the last graph, we don't need to create a new partition for the next graph
        if (i == n-1)
            break;
        
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
        graph = Graph((char *)gfiles[i+1].c_str(), NULL, UNWEIGHTED);
        // Update this partition based on beta
        int nnodes = n2c.size();
        if (beta == 1)          // put all nodes in their on community
            for (int i=0 ; i<nnodes; i++)
                n2c[i]=i;       
        else if (beta > 0) {    // remove the top beta*|V| changed nodes from their parition
            int num_to_move = round(beta*nnodes);
            int max_c = *max_element(n2c.begin(), n2c.end());
            vector<int> csize(nnodes, 0);
            for (int j=0; j<nnodes; j++)
                csize[n2c[j]] ++;
            
            // Move the top changed nodes to a new community
            vector<pair<int,long double> > node_change = rank_change_nodes(gfiles[i], afiles[i], graph, afiles[i+1]);
            for (int j=0; j<num_to_move; j++) {
                int node = node_change[j].first;
                if (csize[n2c[node]] != 1) {
                    csize[n2c[node]] --;
                    n2c[node] = ++ max_c;
                }
            }
            n2c.resize(graph.nb_nodes);
            for (int node=nnodes; node<graph.nb_nodes; node++)
                n2c[node] = node;
            partition_file = dirname + "/part_" + os.str(); 
            write_partition(n2c, partition_file);
        }
    }
    
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
    double elapsed = (end.tv_sec-tot_start.tv_sec)+(double)(end.tv_nsec-tot_start.tv_nsec)/1000000000;
    cout << "Total duration: " << elapsed << endl;
    
    // Cleanup
    delete q;
}
