#include <vector>
#include <functional>
#include <fstream>
#include <sstream>
#include <string.h>
#include <algorithm>
#include <bitset>
#include <string>

#define MAX_DIM 50  // maximum dimension of any attribute vector

using namespace std;
typedef vector<bitset<MAX_DIM> > attrs; // holds one attribute (of dim MAX_DIM) for each node


class Similarity {
    public:
        vector<int> nattrs;
        int size;   // number of nodes
        int ncomms; // number of communities (udpates as algorithm progresses)
        vector<attrs> attrs_set;  // attributes for each node (type x node)
        function<long double(bitset<MAX_DIM>&,bitset<MAX_DIM>&,int)> func;  // similarity function
        vector<long double> comm_sim;   // internal pairwise similarities of the communities
        vector<vector<int> > c2n;       // maps community ids to its corresponding set of nodes for each community
        
        Similarity (string, string);
        
        // Merges the attributes of each community and resets initial conditions (phase 2)
        vector<attrs> aggregate_attrs();
        void initialize(string filename="");
        
        // Pairwise similarity functions
        inline long double sim(int,int);
        inline long double hamming(bitset<MAX_DIM>&,bitset<MAX_DIM>&,int);
        inline long double jaccard(bitset<MAX_DIM>&,bitset<MAX_DIM>&);
        
        // Global similarities
        long double similarity();   
        static long double similarity(vector<vector<int> >&, vector<long double>&);
        
        // Reading input attribute data
        attrs read_attrs(string);
        void read_attrs_set(string);
    
        // Update similarity scores when adding and removing a node to/from a community.
        // Mimics the quality class, but handles similarity changes
        inline void remove(int node, int comm, long double sim_weight);
        inline void insert(int node, int comm, long double sim_weight);
        inline long double gain(int node, int comm, long double sim_weight);
        
};


inline long double
Similarity::sim(int v1, int v2) {
    long double sim = 0;
    unsigned i=0;
    for (; i<attrs_set.size(); i++)
        sim += func(attrs_set[i][v1], attrs_set[i][v2], nattrs[i]);
    return sim/i;
}


inline long double
Similarity::hamming(bitset<MAX_DIM> &attrs1, bitset<MAX_DIM> &attrs2, int dim) {
    return 1-1.0*(attrs1^attrs2).count()/dim;
}


inline long double
Similarity::jaccard(bitset<MAX_DIM> &attrs1, bitset<MAX_DIM> &attrs2) {
    int usize = (attrs1 | attrs2).count();
    if (usize == 0)
        return 1;
    return 1.0*(attrs1 & attrs2).count()/usize;
}


inline void
Similarity::remove(int node, int comm, long double sim_weight) {
    comm_sim[comm] -= sim_weight+1.;
    c2n[comm].erase(std::remove(c2n[comm].begin(), c2n[comm].end(), node), c2n[comm].end());
    if (c2n[comm].size() == 0)
        ncomms --;
}


inline void
Similarity::insert(int node, int comm, long double sim_weight) {
    c2n[comm].push_back(node);
    comm_sim[comm] += sim_weight+1.;
    if (c2n[comm].size() == 1)
        ncomms ++;
}


inline long double
Similarity::gain(int node, int comm, long double sim_weight) {
    int csize = c2n[comm].size();
    long double csim = comm_sim[comm];
    long double nsim = sim_weight+1;

    if (csize == 0)
        return 0;
    return ((csim+nsim)/((csize+1)*(csize+1)) -
            csim/(csize*csize))/(ncomms);
}
