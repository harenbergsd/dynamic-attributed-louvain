Dynamic Attributed Community Detection 
======================================
This repository contains a novel algorithm for performing dynamic attributed community detection in graph data.
The algorithm is based on the [Louvain Algorithm](https://perso.uclouvain.be/vincent.blondel/research/louvain.html) for finding non-overlapping communities in a single graph.
Similarly, our code uses the [authors' original implementation](https://sourceforge.net/projects/louvain/) as a base; their original README located at `README_ORIG.txt`.


Building the program
--------------------
You will need g++ with the c++11 standard, then simply run `make`.


Running the program
-------------------
First, you have to convert the graph(s) to binary format using the `convert` executable. Before doing this, make sure the nodes in your edgelist are are labeled 0 to n-1. Some example data can be found in `data/`.

To convert a graph, use e.g.:
```
$ ./convert -i data/static/karate.graph -o data/static/karate.bin
```

Once you have you graph and attribute data (needs to be on attribute file for each graph file), make a folder consisting of two subfolders: "attributes" and "graphs" (the code, `src/main_louvain.cpp` is hardcoded to look for these). Place the corresponding files in there. Make some part of the files be numbered (e.g., filename_00), because the code will read the files in alphabetically sorted order.

To run, use e.g.:
```
./louvain data/dynamic/ -a 0.5 -b 0.75 -s hamming
```

Several files will be created in the root directory the data is based out of. The `comm_*` files will be the best community partitions at that time step (before nodes were changed based on beta) and the `part_*` files (only created if b!=1) is what is fed into the next iteration (after the change nodes have been moved).
